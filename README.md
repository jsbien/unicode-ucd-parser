The fork of https://github.com/ppablo28/ucd_xml_parser.

ucd_xml_parser
==============

This python script parses NamesList.txt file containing UCD data and
creates a human-readable XML file from them.

Author: Paweł Parafiński